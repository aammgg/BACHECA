//Client LUGLIO 2018
#include "LibreriaAmg.h"

#define PER_ERRORE_LINEA\
		if( CaratteriLinea ==0)	{printf(" RIPETERE  OPERAZIONE NON RIUSCITA: SERVER HA CHIUSO COLLEGAMENTO");getchar();return;}\
		if( CaratteriLinea <0)	{perror("RIPETERE  OPERAZIONE NON RIUSCITA PER  ERRORI LINEA O SERVER");getchar();return;}\
		/*per errore linea si effettua sempre una return che termina l'operazione corrente e attende nuova richiesta di oerazione*/

#define VISUALIZZA_POST_PRIMA_RIGA\
		printf("\n(%4d)   %-8s %-32s  %-76s",Post->NumOrdPost,Post->IdUtente,Post->mittente,Post->dataoggetto);
		
int     CaratteriLinea;  			 // ritorno write\reade su linea.se <1 anomalie
char 	TipoOper;
int 	SocketColloquio; 			//tra server e cliente
utente  Utente;    					// Utente nuovo in registrazione  o gia registrato e loggato
post	*Post;   			  		//per scambi con server con spazio per testo max
int		DimPostMax=sizeof(post)+LMAXTESTO;    	//3124
int 	DimPostMin =sizeof(post);  			 	//124 comprende anche il primo byte del campo testo variabile 
int 	flagloggato=0;   				 		//utente non deve ripetere digitazione credenziali
//======================PROTOTIPI  MODULI FUNZIONI
void AttivazioneCollegamento();
int  LoginCliente();
void Op_Vis_e_Canc_ConNumRiferim();
void OpListaSelettiva();
void OpAcquNuovoPost();
void OpRegistrazioneUt();
void Stringadigitata (char *,int , char *);
int Numerodigitato(char *,int );
void Testomultilineadigitato(); 

/* CODici  EXIT
 * * errori di linea nei moduli operativi determinano solo la fine del colloquio con disconnessione cliente
	0		chiusura normale
	1 		err  socket
	2		err inet_aton
	3 		err connect mpossibile connettersi al server

 */
//***********************************************************
//******             MAIN
//***********************************************************
int main(int argc, char **argv){  
	Post= (post *) malloc(DimPostMax);   
	if(Post == NULL) exit (1);


	while(1)	 	{    // 
		/*smistamento al modulo trattamento tipo operazione 
		 tra una  oper e l'altra il cliente si disconnette e si riconnette solo al momento di inviare i dati*/ 
		while(1){   //ciclo  di attesa tipo oper valido, esce per operazione valida o chiusura clientet con oper F
			printf("\n  BACHECA ELETTRONICA REMOTA CON LOGIN INIZIALE)  ");
			printf("\n1  ELENCO SELETTIVO POSTS");
			printf("\n2  SOLA VISUALIZZAZIONE POST");
			printf("\n3  INVIO NUOVO POST ");
			printf("\n4  REGISTRAZIONE NUOVO UTENTE ");
			printf("\n5  CANCELLAZIONE POST ");
			printf("\nU  LOGOUT DI USCITA DELL'UTENTE");
			printf("\nF  CHIUSURA");
			printf("\n_____________________________________________\n\n");
			printf("\n_DIGITARE  UN TIPO OPERAZIONE VALIDO (CASE SENSITIVE) \n");
			TipoOper=getchar(); if ( TipoOper != '\n') while ( getchar() != '\n' );    // smaltire carat   sino a NL compreso
			if(TipoOper== 'F')	exit (0);   											//chiusura programma
			if(TipoOper=='U') {
				printf ("LOGOUT, INVIO PER NUOVO LOGIN  O  PER CHIUDERE");
				getchar();
				flagloggato=0;
			}
			if(TipoOper>='1' && TipoOper<= '5' ) break;  							 //uscita da while per tipo oper valido
			//se arriva qui ripete
		}
		//smistamento ai moduli applicativi
		switch(TipoOper)
		{
		case '1':
			OpListaSelettiva();
			break;
		case '2':
		case '5':							//entrambi condividdono il modulo
			Op_Vis_e_Canc_ConNumRiferim();
			break;
		case '3':
			OpAcquNuovoPost();
			break;
		case '4':
			OpRegistrazioneUt();
			break;
		}
		close(SocketColloquio);   					//terminato servizio ad un cliente e disconnessione ad esso
		getchar();  								// pausa per  poter leggere i dati  ricevuti
	}
}

//****************************************************************************************
//***   ATTIVAZIONE COLLEGAMENTO COL SERVER CON INVIO  COD OPER   DOPO DIGITAZIONE CAMPI
//****************************************************************************************
void AttivazioneCollegamento(){
	char	indirizzo[19+1]= INDIRIZZOSERVER;
	int esito;
	struct sockaddr_in addr ;
	//---crea socket nel cliente
	SocketColloquio=socket(AF_INET,SOCK_STREAM,0);   //TCP 							<
	if(SocketColloquio <0) {printf ("\nERRORE PROGRAMMA NUMERO  : %d ,INVIO PER CHIUDERE", errno);exit (1);}
	/*MEMO  per impostazione membri sockaddr_in
			sin_family;//Address family (AF_INET) (16 bit)
			sin_port;//porto TCP o UDP(16bit)
			struct in_addr sin_addr;//Indirizzo internet (32 bit)*/
	addr.sin_family=AF_INET;
	esito=inet_aton(indirizzo, &addr.sin_addr) ; 	 // <arpa/inet.h> trasforma indirizzo IP fornito in formato dot
	if(  esito == 0 ){printf ("\nERRORE PROGRAMMA NUMERO  : %d ,INVIO PER CHIUDERE",errno);exit (2);		}
	addr.sin_port =htons (PORT);  	// formato da host to network short 16-bit  <arpa/inet.h>
	//invoca la connessione di un socket su un indirizzo 
	esito=connect (SocketColloquio, (struct sockaddr *) &addr, sizeof (addr));   
	if ( esito < 0)			{
		perror ("IMPOSSIBILE CONNETTERSI AL SERVER  INVIO PER CHIUDERE ");printf("errno%d",errno);exit (3);
	}
	CaratteriLinea=write(SocketColloquio, &TipoOper, sizeof(TipoOper));
	PER_ERRORE_LINEA;
}

//*********************************************************************************************
//******      OPER 1---ESTRAZIONI SELETTIVE
//*********************************************************************************************
void	OpListaSelettiva() {
	int esito,lun, UltimiPostDaEsaminare=0,p=0,ricevuti=0;
	char domanda1[]="  VISUALIZZA N ULTIMI  POSTS IN ARCHIVIO DIGITARE QUANTITA  o 0 pER TUTTI.";
	UltimiPostDaEsaminare=Numerodigitato (domanda1,4); 
	char 	domanda4[]="CERCARE IN USER";
	char StringaRicerU[LUSER+1];
	Stringadigitata (domanda4,sizeof(  StringaRicerU  )-1,StringaRicerU);
	char 	domanda5[]="AND CERCARE IN MITTENTE";
	char StringaRicerM[LMITT+1];
	Stringadigitata (domanda5,sizeof(  StringaRicerM  )-1,StringaRicerM);
	char 	domanda6[]="AND CERCARE IN DATA/OGGETTO ";
	char StringaRicerO[LDATAOGGETTO+1];
	Stringadigitata (domanda6,sizeof(  StringaRicerO  )-1,StringaRicerO);
	char 	domanda7[]="AND CERCARE IN TESTO";
	char StringaRicerT[40+1];
	Stringadigitata (domanda7,sizeof(  StringaRicerT  )-1,StringaRicerT);
	AttivazioneCollegamento();  
	int esitologin= LoginCliente(); 
	if (esitologin <= 0)	return;
	CaratteriLinea=write(SocketColloquio, &UltimiPostDaEsaminare, sizeof(UltimiPostDaEsaminare));
	PER_ERRORE_LINEA;
	CaratteriLinea=write(SocketColloquio, StringaRicerU, sizeof(StringaRicerU));
	PER_ERRORE_LINEA;
	CaratteriLinea=write(SocketColloquio, StringaRicerM, sizeof(StringaRicerM));
	PER_ERRORE_LINEA;
	CaratteriLinea=write(SocketColloquio, StringaRicerO, sizeof(StringaRicerO));
	PER_ERRORE_LINEA;
	CaratteriLinea=write(SocketColloquio, StringaRicerT, sizeof(StringaRicerT));
	PER_ERRORE_LINEA;
	putchar('\n');for(int i=0;i<=160;i++) putchar(	'_');  			 //riga di____
	printf("\n POST SELEZIONATI  ");  
	printf("\nRIFER     USERID    MITTENTE                           DATA e OGGETTO"); 
	putchar('\n');for(int i=0;i<=160;i++) putchar(	'_');   
	p=1;
	char avanti;
	while(1)	{			  									 	//senz fine salvo uscita break per fine lista
		CaratteriLinea=read(SocketColloquio, Post, DimPostMin);		//lettura di sequenza di messaggi di lungh costante e nota di 124 
		PER_ERRORE_LINEA;	
		if(Post->NumOrdPost==-1)break;  						 	// finelista
		ricevuti++;
		VISUALIZZA_POST_PRIMA_RIGA
	}
	putchar('\n');for(int i=0;i<=160;i++) putchar('_'); 		 	 //riga di ____
	printf("\nTROVATI  %d POSTS NON CANCELLATI " ,ricevuti);
}
//***********************************************************************************
//******      OPER 2 SOLO LETTURA --5 CON CODA CANCELLAZIONE CON CONFERMA       MODIFICATO                                                    ******************
//************************************************************************************
void Op_Vis_e_Canc_ConNumRiferim() {
	int lun;
	char 	RigadaRiempire[100+1];
	RigadaRiempire[100]='\0';  
	printf("\n");
	char domanda1[]="NUMERO ->ATTUALE<- DI LISTA PER INDIVIDUAZIONE DEL POST  ";
	int esito;
	int NumReg=	Numerodigitato (domanda1,4);     
	AttivazioneCollegamento();
	int esitologin=	LoginCliente();
	if (esitologin <= 0 ){	return;}
	CaratteriLinea=write(SocketColloquio, &NumReg, sizeof(NumReg));
	PER_ERRORE_LINEA;
	CaratteriLinea=read(SocketColloquio, Post, DimPostMax);
	PER_ERRORE_LINEA;  	 //riceve l'intero post di cui non sa la lunghezza quindi è predisposto alla lunghmzza massima
	//..........ricezione delPOST, con num progr -1 se inesist
	if(Post->NumOrdPost==-1){printf("\n\n\n NON PIU CONSULTABILE  O RIFERIMENTO ERRATO ");return;	}
	putchar('\n');for(int i=0;i<=160;i++) putchar('_');
	printf("\nRIFER     USERID    MITTENTE                            DATA e OGGETTO");   
	VISUALIZZA_POST_PRIMA_RIGA  //stampo la parte fissa
	putchar('\n');for(int i=0;i<=160;i++) putchar('_');
	// VISUALIZZA_TESTO
	printf("\n%s\n",Post->testo);
	if(TipoOper=='2')	{	
		printf ("\n\n INVIO PER NUOVA OPERAZIONE");
		return;
	}
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>segue  cancellazione se tipo5
	
	printf("\n =====");
	printf("\n_CONFERMI LA RIMOZIONE DI QUANTO VISUALIZZATO(CON TIME OUT)?  S/N\n");
	char ConfermaCancellazione=getchar();
	if ( ConfermaCancellazione != '\n') while ( getchar() != '\n' );    // smaltire carat   sino a NL compreso
	CaratteriLinea=write(SocketColloquio, &ConfermaCancellazione, sizeof(ConfermaCancellazione));
	PER_ERRORE_LINEA;
	CaratteriLinea=read(SocketColloquio, &esito, sizeof(esito));
	PER_ERRORE_LINEA;
	printf("\n");
	if(esito==1 )   printf("\nCANCELLATA CONFERMATA ");
	if(esito==0 )   printf("\nRINUNCIA CANCELLAZIONE ");
	if(esito==2 )   printf("\nFALLITA CANCELLAZIONE ");
	if(esito==-31)  printf("\n TEMPO SCADUTO PER CONFERMARE , OPERAZIONE INTERROTTA \n");
	if (esito==-10) printf("\n NON ABILITATO A CANCELLARE");
	return;
}


//******************************************************************************************
//******                         OPER 3 NUOVO POST  
//******************************************************************************************
void OpAcquNuovoPost(){
	printf("\nCOMPILAZIONE TESTO  PRIMA DI CONNETTERSI\n");
	Stringadigitata ("MITTENTE ",LMITT,Post->mittente);
	Stringadigitata ("OGGETTO  ",LDATAOGGETTO,Post->dataoggetto);
	Testomultilineadigitato();
	Post->NumOrdPost=0;   				//lo puo riempire solo il server
	AttivazioneCollegamento();  
	int esitologin= LoginCliente(); 
	if (esitologin <= 0 ){	return;}
	strcpy(Post->IdUtente,Utente.ID);
	CaratteriLinea=write(SocketColloquio, Post, DimPostMax);
	PER_ERRORE_LINEA;
	int conferma = 0;
	CaratteriLinea=read(SocketColloquio, &conferma, sizeof(int));
	PER_ERRORE_LINEA;
	if(conferma == 1){ printf("\nACQUISIZIONE EFFETTUATA");}
	else {printf("\n OPERAZIONE FALLITA PER ERRORI SERVER!!!!!!!!!!!!!!!!!!!!!");}
	return ;
}

//*******************************************************************
//******                 OPER 4 REGISTRAZIONE NUOVO UTENTE
//******************************************************************
void OpRegistrazioneUt(){
	flagloggato=0;
	int esito;
	printf("\n NUOVA REGISTRAZIONE RICHIESTA");
	Stringadigitata ("NUOVO IUSERID",LUSER,Utente.ID);
	Stringadigitata ("PASSWORD ",LPW,Utente.password );
	AttivazioneCollegamento();
	//NESSUN LOGIN che il server per la op4 infatti non attende
	CaratteriLinea=write(SocketColloquio, &Utente, sizeof(Utente));
	PER_ERRORE_LINEA;
	CaratteriLinea=read	(SocketColloquio, &esito, sizeof(esito));
	PER_ERRORE_LINEA;
	if(esito != 0){
		flagloggato=1;
		printf("\nNUOVA REGISTRAZIONE EFFETTUATA ,EFFETTUATO IL LOGIN ");
	}
	else 	 printf("\n UTENTE GIA REGISTRATO");
	return ;
}

//******************************************************************************************
//****                     LOGIN
//*****************************************************************************************
int LoginCliente() {
	int  esito;
 
	/*1 OK ;-1 non OK perche finiti 3 tentativi,0 per timeout*/
	//ritorna 0 anche se  avverte chiusura server per timeout ,errori di line e di archivi
	if (flagloggato==0)printf("\n===LOGIN INIZIALE UTENTE,TENTATIVI.AMMESSI %d  CON TIMEOUT \n ",NTENTLOGIN);
	while(1){
		if (flagloggato==0){    //se eè 1 usa le credenziali gia digitate senza richiederle nuovamente
			Stringadigitata ("IUSERID",LUSER,Utente.ID);
			Stringadigitata ("PASSWORD",LPW,Utente.password );
		}
		CaratteriLinea=write(SocketColloquio, &Utente, sizeof(utente));
		if( CaratteriLinea <0)	{perror("  ERRORI LINEA");getchar();return 0;}
		if( CaratteriLinea ==0)	{printf(" RIPETERE OPERAZIONE NON RIUSCITA: SERVER HA CHIUSO COLLEGAMENTO");getchar();return(0);}
				// chiusura SERVER (Timeot) trattata come non abilitato return=0
		CaratteriLinea=read(SocketColloquio, &esito, sizeof(int));
		if( CaratteriLinea <0)	{perror("  ERRORI LINEA");getchar();return 0;}
		if( CaratteriLinea ==0)	{printf(" RIPETERE  OPERAZIONE NON RIUSCITA: SERVER HA CHIUSO COLLEGAMENTO");getchar();return(0);}
				// chiusura SERVER (Timeot) trattata come non abilitato return=0
		if(esito==1|| esito ==-1 || esito==-30)break;
		printf("\n---UTENTE O PSW ERRATI---\n");
	}
	if(esito==1){ 
		    flagloggato=1;
			printf("\nUETENTE e PSW validi");
	}
	if(esito==-1)		printf("\nESAURITI %d TENTATIVI,INVIO PER NUOVA OPERAZIONE",NTENTLOGIN);
	if(esito==0)		printf("\nERRORE DI LINEA");
	if(esito==-30)		printf("\nETIMEOUT LOGIN,INVIO PER NUOVA OPERAZIONE");
	
	return esito;

}
//----------------------------------------------------------------------
int Numerodigitato (char domanda[],int LungMaxDigita){
	int i,nume,esito; 
	char  ZeroTermStringa='\0', NewLine ='\n';
	char StringaInterna[10];	
	//RICHIESTA DI UN CAMPO CON  GUIDA DIGITAZIONE
	putchar('\n');for(int i=0;i<LungMaxDigita;i++) putchar('_');printf("%s",domanda );	 	 //tetto guida  e stampa richiesta
	putchar('\n');for(int i=0;i<LungMaxDigita;i++) putchar('.');printf("|\r");  			 //puntini guida e ritorno in prima posiz
	char maschera[20];
	sprintf(maschera,"%s%d%s", 	"%",LungMaxDigita ,"d");    //"scrive"  stringa "maschera"  con lunghezza massima del campo
	esito=scanf(maschera,&nume);
	while ( getchar() != NewLine );
	if(esito==0)	nume=0;//per errore carattere o non numerico
	if(nume<0) {
	    printf("\n I numeri negativi non sono validi, assumo zero");
	    nume=0;
	}
	printf("\n       Numero Assunto :(%d)",nume);//conferma di cio che + assunto
	return nume; 

}
//-----------------------------------------------------------------------------------------------
void Stringadigitata (char domanda[],int LungMaxDigita, char StringaEsterna[]){
	char  ZeroTermStringa='\0', NewLine ='\n';
	//RICHIESTA DI UN CAMPO CON  GUIDA DIGITAZIONE
	putchar('\n');for(int i=0;i<LungMaxDigita;i++) putchar('_');printf("%s",domanda );	   //tetto guida   e richiesta
	putchar('\n');for(int i=0;i<LungMaxDigita;i++) putchar('.');printf("|\r");  			//puntini guida e ritorno in prima posiz
	// la stringa puo contenere ANCHE SPAZI ed è diLUNGHEZZA VARIABILE entro il max
	char maschera[20];
	sprintf(maschera,"%s%d%s",  "%",LungMaxDigita ,"[^\n]");    //"scrive"  stringa "maschera"  con lunghezza massima del campo
	StringaEsterna[0]=ZeroTermStringa;    					   // rendo a nulla la  StringaEsterna precedentemente digitata 
	scanf(maschera,StringaEsterna);while ( getchar() != NewLine ); 
	 //esempio "%200[^\n] " significa max 200 car tuttifuorche \n (fine campo),anche spazi (a differenza di %s
}

void Testomultilineadigitato(){ 
	printf("\n TESTO,  PER TERMINARE sequenza ^ INVIO \n");
	//acquisizione testo multilinea con scanf
	Post->testo[0]='\0';
	char maschera[20];
	sprintf(maschera,"%s%d%s",  "%",LMAXTESTO ,"[^^]");    //scrive sulla stringa "maschera" preeedisponendola  con lunghezza appropiata
														//esempio "%2000[^^]" 2000 caratteridi ogni genere,fine campo ^
	scanf(maschera,Post->testo);   						//  fine campo è SOLO il carattere dopo [^ che è ancora un ^;i NL passano
	while ( getchar() != '\n' );   						 //smaltire carat  eccedenti sino a NL compreso
}
