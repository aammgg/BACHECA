//SERVER luglio18
#include "LibreriaAmg.h"

#define PER_ERRORE_LINEA \
		if(CaratteriLinea < 0){\
			perror("ALTRI ERRORI  LINEA  ");printf("errno%d",errno);return;}\
			if(CaratteriLinea == 0){\
				perror("\nIL CLIENTE HA CHIUSO COLLEGAMENTO");printf("errno%d",errno);	return ; }
		/*per errore linea si effettua sempre una return che termina l'operazione corrente e attende nuova richiesta di oerazione*/


FILE	 *fpost,*fUtentiReg;
FILE	 *flogcanc;	 				 //copia dei post cancellati rispetto ultima copia  di Bacheca
FILE 	 *flognuoviPost;  		 	//rcopia dei nuovi post rispetto ultima copia  di Bacheca
int      TotIndiciPostsOccupati=0;  
int      FlagCaricMemoriaValido=0;	//1 quando il gruppo posts è stato caricato.PER IMPEDIRE PRIMA SALVATAGGI DISTRUTTIVI
int     FLAGpasso; 					// per far sapere alla gestione timeout il modulo operativo
int      CaratteriLinea;  			 //su linea caratteri  letti o  scritti ,se<1 errore
int 	 SocketColloquio; 		 	  //tra server e cliente connesso tramite accept
int 	 SocketAscolto;				//sulla porta assegnata
char 	 TipoOper;
int		 DimPostMin=  sizeof(post);    //=124//usata da server e cliente per oper1
int	     DimPostMax=  sizeof(post)+LMAXTESTO;   //=3124// per blocchi su archivo,e opr 3 imda,c, e s,

post *Post, *PostMem[CAPIENZAVETTORE];    
utente   Utente,UtenteRichiedente;
//=======================PROTOTIPI  MODULI FUNZIONI
void Caric_bacheca_disco_in_mem();
int  LoginCliente();
void OpListaSelettiva();
void Op_Vis_e_Canc_ConNumRiferim();
void OpAcquNuovoPost();
void OpRegistrazioneUt();
void Gestore_SIGALRM();
void Gestore_chiusura ();
void CompattaESalva();
void RicostrCanc();
void Gestore_quit();

/*
CODICI DELLE EXIT
*  errori sugli archivi o malloc determinano chiusura server con sola exit senza salvataggio inale
*    utilizzando la procedura  di ricostruzione automatica al riavvio
* errori di linea nei moduli operativi determinano solo la fine del colloquio con disconnessione sercer
* 	0 chiusura normale
	1 errore malloc
	2 errore archivio Bacheca
	3 errore archivio utenti
	4 errore fallito compattamento
	5 errore creazione socket di ascolto su porta
	6 errore bind porta in uso
	7 errore listen
	8 errore creazione socket con cliente per accept
	71 errore arch di log
    90 interruzione brusca per test 
 */
//******************************************************************************
//******    MAIN                                                                     
void main() {
	Post= (post *) malloc(DimPostMax);
	if (Post==NULL) {exit(1);}
	//*****************************************************************
	//*  IMPOSTAZIONE SEGNALI 
	//*****************************************************************
	struct sigaction salrm;
	salrm.sa_handler = &Gestore_SIGALRM; 
	salrm.sa_flags = SA_RESTART; 
	sigfillset(&salrm.sa_mask);
	sigaction(SIGALRM,&salrm,NULL);

	struct sigaction sacc;
	sacc.sa_handler = &Gestore_chiusura;
	sigfillset(&sacc.sa_mask);
	sigaction(SIGHUP,&sacc,NULL);	
	sigaction(SIGINT,&sacc,NULL);
	sigaction(SIGILL,&sacc,NULL);

	struct sigaction squit;
	squit.sa_handler = &Gestore_quit;
	sigfillset(&squit.sa_mask);
	sigaction(SIGQUIT,&squit,NULL);

	//*****************************************************************
	//*  APERTURA PORTA DI ASCOLTO TCP
	//*****************************************************************
	SocketAscolto = socket (AF_INET,SOCK_STREAM,0);    					 //dominio,tipo  tcp
	if (SocketAscolto ==-1)	{
		perror ("\nSOCKET NON PUO ESSERE CREATO");printf("errno%d",errno);exit (5);
	}
	struct sockaddr_in addrServer;   
	memset ( &addrServer, 0, sizeof(addrServer) );// ensures that extra bytes contain 0
	/* sin_family;//Address family (AF_INET) (16 bit)
	sin_port;//porto TCP o UDP(16bit)
	struct in_addr sin_addr;//Indirizzo internet (32 bit)*/
	addrServer.sin_family = AF_INET;
	addrServer.sin_addr.s_addr=htonl (INADDR_ANY);    	 //Formato da host to network long32b <arpa/inet.h> ,INADDR_ANY =qualsiasi indirizzo
	addrServer.sin_port= htons (PORT);					//formato da host to network short 16-bit   ""    "
	int esito;
	//-----Associare alla socket un indirizzo  contattabile  da un cliente
	esito=bind (SocketAscolto, (struct sockaddr *) &addrServer, sizeof (addrServer));
	if (esito<0){
		perror("\nERRORE BIND  ");		
		getchar();   
		exit (6);
	}
	//-----Settare la socket in  stato attesa di richiesta di connessioni
	esito=listen (SocketAscolto,5);  			  // 5 max accodamenti nel kernel a cura listen
	if (esito<0) {
		perror("\n ERRORE listen");printf("errno%d",errno);	exit (7);
	}
	//**************************************************************************************
	//******  ATTIVAZIONE INIZIALE  POSTS IN MEMORIA EV RICOSTRUITO,Backup e due log ***
	//**************************************************************************************
	fUtentiReg=fopen ("UtentiRegistrati","ab+");
	if (fUtentiReg==NULL) 	printf("INSTALLAZIONE UTENTI  CREAT0 VUOT0\n");
	//-
	/*con logagg unito  in append creo una Bacheca completa comprendente pero anche ev record cancellati
	 * che saranno ricancellati  in mem in Base a  logcanc*/
	flognuoviPost=fopen("logagg","rb");
	if(flognuoviPost!=NULL){
		system("cat Bacheca logagg >Ricostruita    \n 	cp Ricostruita Bacheca \n  rm Ricostruita");//ricostruite aggiunte
		printf("\nAggiungo post da log\n");
		if(fclose(flognuoviPost)== EOF)  { 					
		perror("\n errore chiusura file  LogAgg\n");exit(71);
	    }
	    system("rm logagg");
	}
	flognuoviPost=fopen("logagg","ab"); 		//con immagine completa di ogni post aggiunto im memoria
	if(flognuoviPost==NULL){ printf("ERRORE APERTURA LOGGAGG") ;exit(71);}
	Caric_bacheca_disco_in_mem()	;  			 //CARICAMENTO   POSTS  IN MEMORIA  CON ARRAY PUNTATORI  
	flogcanc=fopen("logcanc","rb"); 
	if(flogcanc!=NULL){
		RicostrCanc();
		CompattaESalva(0);
	}
	else{
		flogcanc=fopen("logcanc","ab");
		if(flogcanc==NULL){ printf("ERRORE APERTURA LOGCANC") ;exit(71);}
	}
	system("cp Bacheca Backup");//backup 
	//*************************************************************************************
	//  CICLO    ACCEPT   E smistamento ai moduli opraz ,                
	//*************************************************************************************
	while(1) { // ****************azioni di controllo fatte all'uscita  di un modulo operativo
		printf("ultimo indice utilizzato %d  (-1 equivale a Bacheca vuota) ( indice Massimo Utilizzabile  %d )\n",
				TotIndiciPostsOccupati-1,CAPIENZAVETTORE-1);
		//-------- servizio SEQUENZIALE delle richieste client ricevute,eventualmente accodate
		struct sockaddr_in addrClient; 					  // //structure for the client address
		int lun = sizeof(struct sockaddr_in);
		memset ( &addrClient, 0, sizeof(addrClient) );	//azzeramento opzionale
		/*--------accept estrae  la prima connessione dalla coda di pendenza delle connessioni sulla socket in input
		acquisisce i dati del cliente ed è pronto */
		SocketColloquio = accept(SocketAscolto,(struct sockaddr *) &addrClient,&lun);   
		if (SocketColloquio < 0){perror (" ERRORE CREAZIONE SOCKET");printf("errno%d",errno);exit (8);}
		//riceve il primo messaggio
		CaratteriLinea=read(SocketColloquio, &TipoOper,sizeof(TipoOper));
		PER_ERRORE_LINEA;
		printf ("\n Tipo Operazione %c :",TipoOper);
		int esitologin;
		if(TipoOper!='4') 	{
			esitologin= LoginCliente();					
			if (esitologin<=0)	continue;   			//non validato
		}
		switch (TipoOper)	{
		case '1':
			OpListaSelettiva();
			break;
		case '2':	  									 //lettura
		case '5':	 				  //lettura seguita da cancellazione
			Op_Vis_e_Canc_ConNumRiferim();
			break;
		case '3':
			OpAcquNuovoPost();
			break;
		case '4':
			OpRegistrazioneUt()   ;
			break;
		}

		if(close(SocketColloquio)==-1){
			perror("errore chiusura docket");
			}   
	}   //torna ad attendere un tipo oper da un nuovo  cliente------ciclo senza fine
}
//******   CARICAMENTO DA BACHECA  IN MEMORIA CON   ARRAY PUNTATORI 
void Caric_bacheca_disco_in_mem(){ 
	fpost=fopen ("Bacheca","rb");	
	if (fpost==NULL) {
		fpost=fopen ("Bacheca","ab");	
		if(fclose(fpost)== EOF)  { //Chiusura file e test
		perror("\n errore chiusura file  Bacheca\n");exit(2);
        }
		printf("Prima installazione nessun  post presente\n");// non c'è nulla da caricare
		FlagCaricMemoriaValido=1;     //indica che il gruppo  posts in memoria è correttamente caricato
		return;   
	}
	printf("\nciclo caricamento Bacheca inMEMORIA (se esistente)\n");
	int NumOrdine=0; 
	while(fread(Post,DimPostMax,1,fpost) ){		
		if(ferror(fpost)) {perror("Error: ");exit (2);}
		// lo carico   in memoria
		int DimPost=DimPostMin+strlen(Post->testo);
		PostMem[NumOrdine]= (post *) malloc(DimPost);
		if(PostMem[NumOrdine] == NULL) exit (1);
		//copia tra due struct variabili
		(*PostMem[NumOrdine])= (*Post);   // = copia solo parte dichiarata fissa (124 bytes) senza testo,solo con il suo primo byte per indirizzarlo
		strcpy(PostMem[NumOrdine]->testo,Post->testo);   // testo variabile non contemplata dalla ugualianza suscritta 
		//
		printf("\n(%4d)  %-32s  %-76s ",PostMem[NumOrdine]->NumOrdPost,PostMem[NumOrdine]->mittente,PostMem[NumOrdine]->dataoggetto);
		NumOrdine++;
	}
	if(fclose(fpost)== EOF)  {				 //Chiusura file e test
		perror("\n errore chiusura file  Bacheca\n");exit(2);
	}

	TotIndiciPostsOccupati=NumOrdine;// cioe è il primo indice libero e  la quantita degli indici impegnati
	printf("\n=== %d POSTS NELLA BACHECA \n\n",	TotIndiciPostsOccupati);
	FlagCaricMemoriaValido=1;    //  

}
//°°°°°°°°°°°°°°°°°°°°°°°°GESTORI SEGNALI
void Gestore_SIGALRM(){
	if(FLAGpasso==0)	{			//login
		printf("\n\n TIME OUT Login\n" );
		int invio= -30;
		CaratteriLinea=write(SocketColloquio, &invio, sizeof(int));
		close(SocketColloquio); //permette un errore di read/write 
		}
	if(FLAGpasso==2)	{			//OP 5
		printf("\n\n TIME OUT ATTESA IN op 5 %d \n" ,TipoOper);
		int invio =-31;
		CaratteriLinea=write(SocketColloquio, &invio, sizeof(int));
		close(SocketColloquio); 		//provoca un errore di read/write voluto
		}
	
}
//-----------------------------
void Gestore_chiusura(){
	if(FlagCaricMemoriaValido==0 ){ printf("GRUPPO POSTS MEMORIA NON VALIDO\n ");getchar();exit (91);}
	CompattaESalva(0);
	fclose(flogcanc);
	fclose(flognuoviPost);
	close(SocketAscolto);
	system("rm logcanc \n	rm logagg"); 
	printf("\n*exit per uscire  ");
	getchar();
	exit(0);
}

void Gestore_quit(){
	printf("\nINTERRUZIONE BRUSCA ");
	exit(90);
}


//***********************************************************************
//******     oper 1
//***********************************************************************
void OpListaSelettiva (){   //
	int  esito,UltimiPostDaEsaminare;
	char StringaRicerU[LUSER+1];
	char StringaRicerM[LMITT+1];
	char StringaRicerO[LDATAOGGETTO+1];
	char StringaRicerT[40+1];
	CaratteriLinea=read(SocketColloquio, &UltimiPostDaEsaminare, sizeof(UltimiPostDaEsaminare));
	PER_ERRORE_LINEA;
	CaratteriLinea=read(SocketColloquio, StringaRicerU, sizeof(StringaRicerU));
	PER_ERRORE_LINEA;
	CaratteriLinea=read(SocketColloquio, StringaRicerM, sizeof(StringaRicerM));
	PER_ERRORE_LINEA;
	CaratteriLinea=read(SocketColloquio, StringaRicerO, sizeof(StringaRicerO));
	PER_ERRORE_LINEA;
	CaratteriLinea=read(SocketColloquio, StringaRicerT, sizeof(StringaRicerT));
	PER_ERRORE_LINEA;
	int inizio;
	if (UltimiPostDaEsaminare<=0) inizio=0;///
	else
	{ inizio=TotIndiciPostsOccupati-UltimiPostDaEsaminare;
	if (inizio<0) inizio=0;}
	for(int i=inizio;i<TotIndiciPostsOccupati;i++)	{
		if(PostMem[i]==NULL ) continue;  	 //se cancellato non esiste più PostMem allocato e puntato 
		if(		strstr(PostMem[i]->mittente,StringaRicerM) !=NULL        //strstr verifica se la stringa seconda e' contenuta,  in caso di stringa nulla è sempre contenuta
				&&	strstr(PostMem[i]->dataoggetto,StringaRicerO) !=NULL
				&&	strstr(PostMem[i]->IdUtente,StringaRicerU) !=NULL
				&&	strstr(PostMem[i]->testo,StringaRicerT) !=NULL)	{
			CaratteriLinea=write(SocketColloquio, PostMem[i], DimPostMin);
			//mando il post senza testo (non richiesto),cioè la parte fissa
			//cosi è una sequenza di messaggi di lungh costante  nota di 124 che verranno letti dal cliente per tale lunghezza
			PER_ERRORE_LINEA;
		}
	}
	Post->NumOrdPost=-1;    	//ritornato Un Post  con NumOrdPost-1 se  finelista
	CaratteriLinea=write(SocketColloquio,Post, DimPostMin);
	PER_ERRORE_LINEA;
}	
//************************************************************************************
//******   op 2  e 5
//************************************************************************************
void Op_Vis_e_Canc_ConNumRiferim(){
	int PostCercato;  	 	//PostCercato è il numero di registrazione che identifica il post,parte da 0
	
	FLAGpasso=2;
	CaratteriLinea=read(SocketColloquio, &PostCercato, sizeof(int));
	PER_ERRORE_LINEA;
	if(PostCercato>TotIndiciPostsOccupati ||PostCercato<0  ||PostMem[PostCercato]==NULL) {	
		Post->NumOrdPost=-1;    		//si invia  Un Post  con NumOrdPost -1 se  inesistente
		CaratteriLinea=write(SocketColloquio,Post, DimPostMax);
		PER_ERRORE_LINEA;
		return ;
	}
	int	DimPost=  DimPostMin+strlen(PostMem[PostCercato]->testo);    //calcolo la dimensione esatta da prelevare e inviare 
	CaratteriLinea=write(SocketColloquio, PostMem[PostCercato],DimPost );		//invio post cercato
	PER_ERRORE_LINEA;
	if(TipoOper!='5') return;   
	// coda per cancellazione SOLO se tipo5
	alarm(TIMEOUTCANC);
	char  ConfermaRichiestaCancel; 
	CaratteriLinea=read(SocketColloquio, &ConfermaRichiestaCancel, sizeof(ConfermaRichiestaCancel));
	alarm(0);				//ricevuta la conferma o rinumcia
	PER_ERRORE_LINEA;
	int esito;
	int abilitato=strcmp(PostMem[PostCercato]->IdUtente,UtenteRichiedente.ID);  //se zero l'utente e' abilitato a cancellare
	if(abilitato!=0){        // se l'utente e' sbagliato setto esito e salta alla write
		esito=-10;
	}	
	else{
		if(ConfermaRichiestaCancel!='S') esito=0;//non deve cancellare
	    else{
		esito=1; 							//1 =ok 2 errori 0 rinuncia
		fwrite(PostMem[PostCercato],DimPostMax,1,flogcanc);  //aggiorno log
		    if(ferror(flogcanc)){	
			    printf("ERRORE SCRITTUTA LOGCANC: ");
			    esito=2;
			    CaratteriLinea=write(SocketColloquio, &esito, sizeof(int));  
			    return ;// log  non riuscito,cancellazione non effettuata e transazione abortita ritornado errore al cliente
		    } 
	        fflush(flogcanc);
	        printf("%d<<<nfree canc\n",1);
	        free(PostMem[PostCercato]);   			// libera lo spazio allocato per il post
	        PostMem[PostCercato]=NULL;  				 //camcella il puntatore
	        }
	    }
	CaratteriLinea=write(SocketColloquio, &esito, sizeof(esito));
	PER_ERRORE_LINEA;
	FLAGpasso=-1;
	return;
}
//*************************************************************************
//******     OPERAZIONE 3
//************************************************************************
void OpAcquNuovoPost(){
	//  se saturato l'indice del vettore azione pulizia con modifica dei riferimenti ai posts
	if(TotIndiciPostsOccupati>=CAPIENZAVETTORE ){
		printf("\n-------------------saturo il puntatore >>non adisponibilili ulteriori indici per aggiunta,\n");
		CompattaESalva(MARGINE);  	 //crea  posizioni libere eliminando parte dei vecchi(i primi)
	}
	//******************SCRITTURA SU FILE  e in MEMORIA CON RELATIVI PUNTATORI********
	int esito ; 			//1 ok,0 errore di malloc o di scrittura log
	CaratteriLinea=read(SocketColloquio, Post, DimPostMax);
	PER_ERRORE_LINEA;
	//allocazione spazio
	int DimPost=DimPostMin+strlen(Post->testo);
	PostMem[TotIndiciPostsOccupati]= (post *) malloc(DimPost);
	if(PostMem[TotIndiciPostsOccupati] == NULL)	{ 
		printf("\n errore malloc con chiusura operzione fallita dopo aavviso al cliente");
		esito =0;
		CaratteriLinea=write(SocketColloquio, &esito, sizeof(int));  // ritorna al cliente  errore 0 e poi si chiude
		exit (1);}
		
	Post->NumOrdPost =TotIndiciPostsOccupati;            
	//copia tra due struct variabili
	(*PostMem[TotIndiciPostsOccupati])= (*Post);   					//copia solo parte dichiarata fissa  
	strcpy(PostMem[TotIndiciPostsOccupati]->testo,Post->testo);   // copia testo variabile, non previsto dall'uguaglianza precedente
	//scrive log 
	fwrite(PostMem[TotIndiciPostsOccupati],DimPostMax,1,flognuoviPost);
	if(ferror(flognuoviPost))
	{
		printf("\n errore scrittura log con chiusura operazione  fallita dopo aavviso al cliente");
		esito =0;// log  non riuscito,cancellazione non effettuata e transazione abortita 
		CaratteriLinea=write(SocketColloquio, &esito, sizeof(int));  // mando al  al client di errore e poi si chiude
		exit (71);// il post gia in memoria ma non loggabile sparira con la ricostruzione dopo chiusura perche non loggato
				}
	fflush(flognuoviPost);
	TotIndiciPostsOccupati++ ;
	esito = 1;
	CaratteriLinea=write(SocketColloquio, &esito, sizeof(int));  
	PER_ERRORE_LINEA; 
	printf ("\n aggiunto post  \n");
}

//*******************************************************************************
//******       oper 4
//*******************************************************************************
void OpRegistrazioneUt(){
	CaratteriLinea=read(SocketColloquio, &UtenteRichiedente, sizeof(utente));
	PER_ERRORE_LINEA;
	int esito;   //0 errore di esitenza 1 0k
	rewind(fUtentiReg);
	while(1)	{    //loop senza fine,break per trovato o fine file=non gia registrato
		fread(&Utente,sizeof(utente),1,fUtentiReg);
		if(ferror(fUtentiReg)){perror("Error Utenti: "); exit (3);}   //lettura file utenti
		if(feof(fUtentiReg))  {esito=1;break;}   			//ok ,non presente in archivio 
		esito=strcmp(Utente.ID,UtenteRichiedente.ID); // 0 per uguaglianza
		if (esito==0 ){	break;}   			//esistente quindi errore,altrimenti nuovo giro
	}
	if (esito ==  1){   //ok
		if(fseek(fUtentiReg,0,SEEK_END)!=0){   //si posiziona per scrivere in coda al file  
			printf("errore di seek su archivio utenti");exit(3);
		}  			

		fwrite(&UtenteRichiedente,sizeof(utente),1,fUtentiReg);
		if(ferror(fUtentiReg)){perror("Error: "); exit (3);}   
		fflush	(fUtentiReg);  					 //per sollecitare scaricamento buffer
	}
	CaratteriLinea=write(SocketColloquio, &esito, sizeof(esito));
	PER_ERRORE_LINEA;
	return ;
}
//-------------------------------------------------

void CompattaESalva(int marginerichiesto){
	int inuovo=0;
	if (marginerichiesto<0) marginerichiesto=0; 
	printf("\n COMPATTAMENTO INDICE>>>>>>ELIMINATIe %d POSTS PIU ANTICHI>>>>",marginerichiesto);
	for( int i=0;i<marginerichiesto;i++) {   //elimina i piu antichi
		printf("\n elim (%4d)  ",i);
		if(PostMem[i]!=NULL) free(PostMem[i]);   //puntatore verso non cancellato
		PostMem[i]=NULL;   //reset puntatore
	}
	if(FlagCaricMemoriaValido==0){printf("\nTABELLA NON PRESENTE,NESSUN SALVATAGGIO");getchar(); return;}   
	printf("\nSALVATAGGIO\n");
	fpost=fopen("Backup","wb");
	if (fpost==NULL) {printf("\n errore creazione file backup");exit(4);}
	for( int i=marginerichiesto;i<TotIndiciPostsOccupati;i++) { 	//num tot di indici usati anche per posts cancellati
		if(PostMem[i] == NULL){printf("\nex %d cancellato",i); continue;}   //salto i nulli , sono i cancellati 
		PostMem[inuovo]=PostMem[i];   			   //inuovo parte da zero ,qui copio i puntatori
		if(marginerichiesto!=0) PostMem[i]=NULL;               
		PostMem[inuovo]->NumOrdPost=inuovo;          // aggiorno anche l'indice salvato nella struttura del post 
		fwrite(PostMem[inuovo],DimPostMax,1,fpost);   // Contemporaneamente salvo su disco 
		if(ferror(fpost)){ perror("Error: ");exit (4);}
		printf("\n%d-> \t(%4d)  %-32s  %-76s",i,inuovo,PostMem[inuovo]->mittente,PostMem[inuovo]->dataoggetto);
		inuovo++;
	}
	if(fclose(fpost)== EOF) { //Chiusura file e test
		perror("\n errore DI chiusura file  Backup\n");exit(4);
	}

	printf("\n >>>>scaricati %d post in bacheca",inuovo);
	TotIndiciPostsOccupati=inuovo;
	printf("\nAZZERAMENTO LOGS\n");
	// aggiornato il backup tento la copia  su nuova Bacheca
	//un errore mantiene valida la vecchia Bacheca e si chiude il  server in vista del ripristino automatico
	if(system("cp Backup Bacheca ")==-1){  
		printf("FALLITO COPIA PER COMPATTAMENTO"); exit(4);
	}
	if(fclose(flogcanc)== EOF)  { //Chiusura file e test
		perror("\n errore chiusura file  LogCanc\n");exit(71);
	    }
	if(fclose(flognuoviPost)== EOF)  { //Chiusura file e test
		perror("\n errore chiusura file  LogAgg\n");exit(71);
	    }
	system("rm logcanc \n	rm logagg"); 
	flognuoviPost=fopen("logagg","ab");  		// immagine completa di ogni post aggiunto im memoria
	if(flognuoviPost==NULL){ printf("ERRORE APERTURA LOGGAGG") ;exit(71);}
	flogcanc=fopen("logcanc","ab"); 		 // immagine completa di ogni post cancellato in mem
	if(flogcanc==NULL){ printf("ERRORE APERTURA LOGCANC") ;exit(71);}

}
//**********************************************************************************
//******                        LOGIN
//**********************************************************************************
int LoginCliente(){
	int esito ;// 1 registrato  - non registrato
	FLAGpasso=0;
	alarm(TIMEOUTlogin);
	/*ovunque avvenga il timeout che chiude il socket e alla prima operazione sul socket 
	 * si riceve un errore read(write gestito come fine per non trovato)*/
	for(int i=1;i<=NTENTLOGIN;i++){	
		esito=0;
		rewind(fUtentiReg);
		CaratteriLinea=read(SocketColloquio, &UtenteRichiedente, sizeof(utente));
		if( CaratteriLinea <= 0){	alarm(0);return 0;}   		 //fallita ricezione trattata come non trovato 0
		while(1)	{    //loop senza fine salvo break= trovato o fine =non registrato
			fread(&Utente,sizeof(utente),1,fUtentiReg);
			if(ferror(fUtentiReg)){perror("Error utenti: "); exit (3);}   //lettura file utenti
			if(feof(fUtentiReg)){ printf("\n autenticazione fallita"); break;} 		//fine file 
			if (strcmp(Utente.ID,UtenteRichiedente.ID)==0 && strcmp(Utente.password,UtenteRichiedente.password)==0){
				esito=1; //comparazione ok vale 0
				break;   //esce dal ciclo corrente con trovato
			}
		}   //fine  di un tentativo validazione 
        if(esito==0 && i==NTENTLOGIN){esito=-1;}
		CaratteriLinea=write(SocketColloquio, &esito, sizeof(esito));
		if( CaratteriLinea <= 0){	alarm(0);return 0;} //fallito invio ,trattato come non trovato 
		if(esito == 1)	break;  
	}   //FUORI DAL LOOP dei 3 TENTATIVI
	alarm(0);
	FLAGpasso=-1;
	return esito;   
}
//-----------------------------------------------
void RicostrCanc(){
	printf("\nRIAPPLICAZIONE CANCELLAZIONI DA LOG");
	int numdacance=0,recordletto=0;
	while(fread(Post,DimPostMax,1,flogcanc)){	
		if(ferror(flogcanc)){ perror( "ERRORE LETT POST");return;}
		numdacance=Post->NumOrdPost;
		printf("\n numero letto nel log cancellazioni %d ",numdacance);
		if((numdacance==PostMem[numdacance]->NumOrdPost) && (PostMem[numdacance]!=NULL) ){ 
				free(PostMem[numdacance]);  	 // libera lo spazio allocato per il post
				PostMem[numdacance]=NULL;		//cancella il suo indirizzo puntatore
			    printf("\n <>(%d)<<<Ricancellatonel \n",numdacance);  
			
			//free(PostMem[numdacance]);   
			//PostMem[numdacance]=NULL;   
		}
		else  printf("\n ERRORE********* con la cancellazione del post numero  %d",numdacance );
		recordletto++;
	}
	printf("\nTOTALI CANCELLAZIONI TROVATE %d\n ",recordletto);
}
