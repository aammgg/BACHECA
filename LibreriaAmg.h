//LibreriaAmg.h
#include  <unistd.h> 
 /*<unistd.h>provides access to the POSIX operating system API;
 * ora solo consigliato per evitare  cob -W : implicit declaration of function read,wrire,close,alarm*/
#include  <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>   
#include <signal.h> 
#include <arpa/inet.h>   //htons(),htonl(),inet_aton 
//parametri connessione
#define PORT 25000
//#define INDIRIZZOSERVER "192.168.1.10" provato OK in LAN del routerdi casa  letto con ipconfig
#define INDIRIZZOSERVER "127.0.0.1" //local host
// parametri del gruppo  posts
#define CAPIENZAVETTORE 14  //  e' la capienta dell'array che contiene i post 
#define MARGINE   4  //  e' il numero di post + vecchi che cancella al compattamento  

//parametri lungh dati
#define LPROGR 4  // numero di byte del'int identificativo del post 
#define LUSER 8
#define LMITT 32
#define LDATAOGGETTO 76
#define LMAXTESTO 3000 
#define LPW 8
//time out e altri limiti 
#define TIMEOUTCANC 15  //per confermare cancellazione op5
#define TIMEOUTlogin 15  // per completare login
#define NTENTLOGIN 3   //num max tentativi LOGIN 
typedef struct{  //lungh 124--3124
	int  NumOrdPost;  //inviato al cliente come chiave di riferimento posizionale a un post,in cui è contenuta
	char IdUtente[LUSER+1];    
	char mittente[LMITT+1];
	char dataoggetto[LDATAOGGETTO+1];
	/*  123 bytes è la somma dei primi quattro membri
	 *  sizeof(Post)  è solo 124 byte,corrispondenti 123+1 bytes ,
	 *  compreso cioè il primo byte del testo necessario per disporre del suo indirizzo
	 * le dimensioni da impegnare con malloc  sono sizeof(post)+  lungh  variabile testo 
	 * (max LMAXTESTO) 
	 * */
	char testo[]; //Flexible array member che rende variabile la dimensione della intera struttura;
}post;
typedef struct{  //lungh 18
	char	 ID [LUSER+1];
	char      password[LPW+1];
}utente;

